require 'state_machine'
require 'highline/import'

class PopVehicle
  state_machine :initial => :parked do

    event :park do
      transition [:idling] => :parked
    end

    event :ignite do
      transition [:parked] => :idling
    end

    event :gear_up do
      transition [:idling] => :first_gear,
                 [:first_gear] => :second_gear,
                 [:second_gear] => :third_gear
    end

    event :gear_down do
      transition [:first_gear] => :idling,
                 [:second_gear] => :first_gear,
                 [:third_gear] => :second_gear
    end
    event :crash do
      transition [:first_gear, :second_gear, :third_gear] => :crashed
    end

    event :repair do
      transition [:crashed] => :parked
    end
  end
end

vehicleA = PopVehicle.new
vehicleB = PopVehicle.new

while true do
  puts "Le vehicule A est #{vehicleA.state}"
  puts "Le vehicule A peut #{vehicleA.state_events}"
  input = ask "action A ? :"
  vehicleA.send(input)
  puts ""
  puts "Le vehicule B est #{vehicleB.state}"
  puts "Le vehicule B peut #{vehicleB.state_events}"
  input = ask "action B ? :"
  vehicleB.send(input)

end
